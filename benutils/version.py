""" benutils/version.py """
__version__ = "0.2.0"

# 0.2.0   (13/12/2019): Add support of signal/slot facilities in pure python
#                       using signalslot package.
# 0.1.5   (31/10/2019): Correct error in sdr/* files
# 0.1.4   (19/07/2019): Add usefull method to misc/datacontainer.py
# 0.1.3   (19/03/2019): Moves to PyQt5
# 0.1.2   (13/03/2019): Add initial condition to 'sdr files'
# 0.1.1   (27/02/2018): Add classes of handler for data logging mechanism
# 0.1.0a0 (20/02/2018): Initial version
